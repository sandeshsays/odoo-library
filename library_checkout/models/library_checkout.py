from odoo import api, fields, models

class Checkout(models.Model):
    _name = "library.checkout"
    _description = "Checkout Request"
    member_id = fields.Many2one("library.member",
                                required=True)
    user_id = fields.Many2one("res.users",
                              "Librarian",
                              default=lambda s: s.env.user,)
    request_date = fields.Date(default=lambda s: fields.Date.today(),)
    line_ids = fields.One2many("library.checkout.line",
                               "checkout_id",
                               string="Borrowed Books", )

    _inherit = ["mail.thread", "mail.activity.mixin"]

    @api.model
    def _default_stage_id(self):
        Stage = self.env["library.checkout.stage"]
        return Stage.search([("state", "=", "new")],
                            limit=1)

    @api.model
    def _group_expand_stage_id(self, stages, domain,
                               order):
        return stages.search([], order=order)

    stage_id = fields.Many2one(
        "library.checkout.stage",
        default=_default_stage_id,
        group_expand="_group_expand_stage_id")
    state = fields.Selection(related="stage_id.state")
    checkout_date = fields.Date(readonly=True)
    close_date = fields.Date(readonly=True)

    @api.model
    def create(self, vals):
       new_record = super().create(vals)
       if new_record.stage_id.state in ("open", "close"):
           raise exceptions.UserError(
               "State not allowed for new checkouts."
           )
       return new_record

    def write(self, vals):
        if "stage_id" in vals:
            Stage = self.env["library.checkout.stage"]
            old_state = self.stage_id.state
            new_state = Stage.browse(vals["stage_id"]).state
            if new_state != old_state and new_state == "open":
                vals['checkout_date'] = fields.Date.today()
            if new_state != old_state and new_state == "done":
                vals['close_date'] = fields.Date.today()
        super().write(vals)
        return True

    def button_done(self):
        Stage = self.env["library.checkout.stage"]

        done_stage = Stage.search([("state", "=", "done")], limit=1)
        for checkout in self:
            checkout.stage_id = done_stage
        return True


