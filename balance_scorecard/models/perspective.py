from odoo import models, fields, api


class Perspective(models.Model):
    _name = 'balance.perspective'
    _description = ' Perspective Lists'

    #stringfields

    name = fields.Char('Title')
    description = fields.Text()
    scorecard_id = fields.Many2one("balance.scorecard",
                                      string="Scorecard")

