from odoo import models, fields, api


class Objective(models.Model):
    _name = 'balance.objective'
    _description = 'Balance Objective Lists'

    # stringfields
    name = fields.Char()
    description = fields.Text()
    perspective_id = fields.Many2one("balance.perspective",
                                   string="Perspectives")
