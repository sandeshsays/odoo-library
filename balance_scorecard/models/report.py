from odoo import models, fields, api


class Organization(models.Model):
    _name = 'mst.report'
    _description = 'Report Lists'

    # stringfields
    name = fields.Char('Title')
    content = fields.Char()
    date = fields.Date()
