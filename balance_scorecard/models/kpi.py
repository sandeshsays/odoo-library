from odoo import models, fields, api


class Objective(models.Model):
    _name = 'balance.kpi'
    _description = 'Balance Kpi Lists'

    # stringfields
    name = fields.Char()
    description = fields.Text()
    objective_id = fields.Many2one("balance.objective",
                                   string="Objectives")
    target = fields.Float()
    threshold = fields.Float()
