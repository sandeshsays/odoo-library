from odoo  import models, fields, api


class Kpireport(models.Model):
    _name = 'balance.kpireport'
    _description = 'Kpi Report List'

    #stringfields

    kpi_id = fields.Many2one("balance.kpi",
                                   string="Kpi")
    report_id = fields.Many2one("mst.report",
                                   string="KpiReport")
