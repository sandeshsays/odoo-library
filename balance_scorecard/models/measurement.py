from odoo import models, fields, api


class Measurement(models.Model):
    _name = "balance.measurement"
    _description = "Measurement Lists"

    # string Fields
    kpi_id = fields.Many2one("balance.kpi",
                             string="Kpi")
    date = fields.Date()
    actual_value = fields.Float()

