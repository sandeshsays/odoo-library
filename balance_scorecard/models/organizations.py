# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Organization(models.Model):
    _name = 'mst.org'
    _description = 'Organizations Lists'

    # stringfields
    name = fields.Char()
    description = fields.Text()
