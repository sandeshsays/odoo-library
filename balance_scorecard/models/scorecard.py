from odoo import models, fields, api


class Scorecard(models.Model):
    _name = 'balance.scorecard'
    _description = 'Balance ScoreCard Lists'

    # stringfields
    name = fields.Char('Title')
    description = fields.Text()
    organization_id = fields.Many2one("mst.org",
                                   string="Organizations")
